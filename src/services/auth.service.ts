import * as firebase from 'firebase';

export class AuthService {

    signUpData(email, password) {
        // console.log(user);
        return firebase.auth().createUserWithEmailAndPassword(email, password);
    }

    signInData(email, password) {
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }
    logout() {
        firebase.auth().signOut();
    }
    getActiveUser() {
        return firebase.auth().currentUser;
    }
}