import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class CartService {
    constructor(private http: HttpClient) {

    }
    addOrder(order) {
       return this.http.post('https://woocommerce-5a98e.firebaseio.com/orders.json', order);
        // console.log(order);
    }
}