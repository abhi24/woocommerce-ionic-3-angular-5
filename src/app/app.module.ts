import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HttpClientModule } from '@angular/common/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuPage } from '../pages/menu/menu';
import { ProductByCategoryPage } from '../pages/product-by-category/product-by-category';
import { ProductDetailsPage } from '../pages/product-details/product-details';
import { IonicStorageModule } from '@ionic/storage';
import { CartPage } from '../pages/cart/cart';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { AuthService } from '../services/auth.service';
import { SignInPage } from '../pages/sign-in/sign-in';
import { CheckoutPage } from '../pages/checkout/checkout';
import { CartService } from '../services/cart.service';
import { SearchPage } from '../pages/search/search';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MenuPage,
    ProductByCategoryPage,
    ProductDetailsPage,
    CartPage,
    SignUpPage,
    SignInPage,
    CheckoutPage,
    SearchPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MenuPage,
    ProductByCategoryPage,
    ProductDetailsPage,
    CartPage,
    SignUpPage,
    SignInPage,
    CheckoutPage,
    SearchPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CartService,
    AuthService,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
