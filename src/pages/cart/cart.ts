import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SignInPage } from '../sign-in/sign-in';
import { CheckoutPage } from '../checkout/checkout';


@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  cartItems = [];
  total = 0;

  constructor(private viewCtr: ViewController, private storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
    this.storage.ready().then(() => {
      this.storage.get('cart').then(data => {
        this.cartItems = data;
        this.cartItems.map(data => this.total += data.price)
      })

    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

  changeQty(item, index, val) {
    this.storage.get('cart').then(data => {
      for (let i = 0; i < data.length; i++) {
        if (data[i].product.index === item.product.index) {
          data[i].qty = data[i].qty + val;
          if (val == -1) {
            data[i].price = data[i].price - 2
          } else {
            data[i].price = data[i].price + 2
          }
        }
      }
      this.storage.remove('cart');
      this.storage.set('cart', data).then(() => {
        // console.log(data);
        this.storage.get('cart').then(data => {
          this.cartItems = data;
          this.total = 0;
          this.cartItems.map(data1 => this.total += data1.price)
        })
      })
    })
  }

  removeFromCart(item, index) {
    const ite = this.cartItems.filter((itemOf) => {
      return itemOf.product.index !== item.product.index
    })
    this.cartItems = ite;
    this.storage.remove('cart');
    this.storage.set('cart', this.cartItems).then((data) => {
      this.total = 0;
      this.cartItems.map(data1 => this.total += data1.price)
    })
  }

  closeModal() {
    this.viewCtr.dismiss();
  }

  checkout() {
    this.storage.get('loggedUser').then(data => {
      if (data !== null) {
        this.navCtrl.push(CheckoutPage);
      } else {
        this.navCtrl.push(SignInPage, { next: CheckoutPage });
      }
    })
  }

}
