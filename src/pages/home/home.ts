import { Component, ViewChild } from '@angular/core';
import { NavController, Slides, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { ProductDetailsPage } from '../product-details/product-details';
import { SearchPage } from '../search/search';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  products;
  finalProducts;
  // searchQuery = '';
  @ViewChild('prodSlides') prodSlides: Slides;
  constructor(private toastCtrl: ToastController, private http: HttpClient, public navCtrl: NavController) {
    this.http.get('https://woocommerce-5a98e.firebaseio.com/products.json').subscribe(data => {
      this.products = data;
      this.finalProducts = this.products.filter((data) => {
        return data.index < 5
      })
    })
  }

  ionViewDidLoad() {
    setInterval(() => {
      if (this.prodSlides.getActiveIndex() == this.prodSlides.length() - 1) {
        this.prodSlides.slideTo(0);
      }
      this.prodSlides.slideNext();
    }, 1800);
  }

  onSearch(eve) {
    console.log(eve.target.value);
    if (eve.target.value.length >= 1) {
      this.navCtrl.push(SearchPage, { 'searchQuery': eve.target.value });
    } else {
      this.toastCtrl.create({
        message: 'Enter Something',
        duration: 1000
      }).present();
    }
  }

  loadMoreProd(event) {
    // console.log('I amhere');
    this.http.get('https://woocommerce-5a98e.firebaseio.com/products.json').subscribe(data => {
      // console.log(data);
      // console.log(this.products);
      this.products = this.products.concat(data);
      // console.log(this.products);
    })
  }

  itemClicked(item) {
    console.log(item);
  }

  openProductPage(prod) {
    this.navCtrl.push(ProductDetailsPage, prod);
  }

}
