import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  searchQuery;
  finalData;
  newArr = [];
  constructor(private http: HttpClient, public navCtrl: NavController, public navParams: NavParams) {
    this.searchQuery = this.navParams.data.searchQuery;
  }

  ionViewDidLoad() {
    this.http.get('https://woocommerce-5a98e.firebaseio.com/products.json/').subscribe(data => {
      console.log(data);
      this.finalData = data;
      for (let i = 0; i < this.finalData.length; i++) {
        // console.log(this.finalData[i].name.includes('t'));
        if (this.finalData[i].name.includes(this.searchQuery)) {
          this.newArr.push(this.finalData[i]);
        }
      }
      console.log(this.newArr);
    })
  }

}
