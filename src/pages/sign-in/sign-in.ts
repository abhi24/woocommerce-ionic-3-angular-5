import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { AuthService } from '../../services/auth.service';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

  constructor(private toastCtrl: ToastController,
    private alertCtrl: AlertController, private storage: Storage, private authSer: AuthService, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignInPage');
  }

  onSubmit(signUpForm) {
    console.log(signUpForm.value);
    this.authSer.signInData(signUpForm.value.email, signUpForm.value.password)
      .then(data => {
        console.log(data);
        this.storage.set('loggedUser', data);
        this.authSer.getActiveUser().getToken().then(data => {
          console.log(data);
          this.storage.set('loggedIn', data);
          this.alertCtrl.create({
            title: 'Login Successful',
            message: 'You have been logged in',
            buttons: [{
              text: 'Ok',
              handler: () => {
                if (this.navParams.get('next')) {
                  this.navCtrl.push(this.navParams.get('next'));
                } else {
                  this.navCtrl.pop();
                }
              }
            }]
          }).present()
        });
      })
      .catch(data => {
        this.toastCtrl.create({
          message: 'Invalid Details',
          duration: 1000
        }).present();
        console.log('Invalid Details')
      })

  }

}
