import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CartPage } from '../cart/cart';


@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage {
  product: any;
  constructor(private modelCtrl: ModalController, private toastCtrl: ToastController, public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.product = this.navParams.data;
    // console.log('ionViewDidLoad ProductDetailsPage', this.product);
  }
  clearStorage() {
    this.storage.clear();
    // console.log('jere');
  }

  openCart() {
    // console.log('Open Cart');
    this.modelCtrl.create(CartPage).present();
  }
  addToCart(prod) {
    // console.log(prod);
    // this.storage.clear();
    this.storage.get('cart').then(data => {
      // console.log(data);
      if (data == null || data.length == 0) {
        console.log('here');
        data = [];
        data.push({
          'product': prod,
          'qty': 1,
          'price': 2
        })
      } else {
        let added = 0;
        for (var i = 0; i < data.length; i++) {
          if (data[i].product.index === prod.index) {
            data[i].qty += 1;
            data[i].price += 2
            added = 1;
          }
        }
        console.log(added);
        if (added == 0) {
          data.push({
            'product': prod,
            'qty': 1,
            'price': 2
          })
        }
      }

      this.storage.set('cart', data).then(() => {
        // console.log(data);
      })
    })
    const toast = this.toastCtrl.create({
      message: 'Cart Updated',
      duration: 1000
    })
    toast.present();
  }

}
