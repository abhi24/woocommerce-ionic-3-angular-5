import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { AuthService } from '../../services/auth.service';
import { SignInPage } from '../sign-in/sign-in';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  constructor(private alertCtrl: AlertController, private toastCtrl: ToastController, private authSer: AuthService, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }

  onSubmit(signUpForm) {
    console.log(signUpForm.value);
    this.authSer.signUpData(signUpForm.value.email, signUpForm.value.password).then(data => {
      this.alertCtrl.create({
        title: 'Account created',
        message: 'Please login to proceed',
        buttons: [{
          text: 'Login',
          handler: () => {
            this.navCtrl.push(SignInPage)
          }
        }]
      }).present();
    }).catch(error => {
      console.log(error.message);
      const toast = this.toastCtrl.create({
        message: error.message,
        duration: 1000
      });
      toast.present();
    })

  }

}
