import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CartService } from '../../services/cart.service';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  paymentMethods: any[];
  allDetails = {};
  constructor(
    private alertCtrl: AlertController,
    private cartSer: CartService,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.paymentMethods = [
      { method_id: "bacs", method_title: "Direct Bank Transfer" },
      { method_id: "cheque", method_title: "Cheque Payment" },
      { method_id: "cod", method_title: "Cash on Delivery" },
      { method_id: "paypal", method_title: "PayPal" }];
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
  }

  onSubmit(userFomr) {
    this.storage.get('cart').then(data => {
      this.allDetails = {
        cart: data,
        userDetail: userFomr.value
      };
      this.cartSer.addOrder(this.allDetails).subscribe(data => {
        console.log(data);
        this.alertCtrl.create({
          title: 'Order Placed',
          message: 'Order number is ' + data['name'],
          buttons: [{
            text: 'Ok',
            handler: () => {
              this.navCtrl.setRoot(HomePage)
            }
          }]
        }).present()
      });
    })
  }

}
