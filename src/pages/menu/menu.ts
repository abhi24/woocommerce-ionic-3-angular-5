import { Component, Injectable } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HttpClient } from '@angular/common/http';
import { ProductByCategoryPage } from '../product-by-category/product-by-category';
import { SignUpPage } from '../sign-up/sign-up';
import { SignInPage } from '../sign-in/sign-in';
import { AuthService } from '../../services/auth.service';
import { Storage } from '@ionic/storage';
import { CartPage } from '../cart/cart';

@Injectable()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  homePage = HomePage;
  categories;
  finalCate;
  token = false;
  user: any;
  constructor(private storage: Storage,
    private authSer: AuthService,
    private http: HttpClient,
    public navCtrl: NavController,
    public navParams: NavParams, private menuCtrl: MenuController) {
    this.user = {};
  }

  ionViewDidEnter() {
    this.http.get('https://woocommerce-5a98e.firebaseio.com/categories.json').subscribe(data => {
      this.categories = data;
      this.finalCate = this.categories.filter((data1) => {
        return data1.parent === 0;
      });

      this.storage.get('loggedIn').then(data => {
        this.user = data;
        this.storage.get('loggedUser').then(data1 => {
          this.user = data1.email;
        });
        // console.log(this.user);
        if (data) {
          this.token = true
        } else {
          this.token = false;
        }
      })
      console.log(this.finalCate);
    })
    console.log('ionViewDidLoad MenuPage');
  }

  itemClicked(item) {
    console.log(item);
    this.navCtrl.push(ProductByCategoryPage, item);
  }

  openPage(req) {
    if (req === 'signUp') {
      this.navCtrl.push(SignUpPage);
    } else if (req === 'cart') {
      this.navCtrl.push(CartPage);
    } else {
      this.navCtrl.push(SignInPage);

    }
    this.menuCtrl.close();
  }

  logout() {
    this.token = false;
    this.authSer.logout();
    this.storage.remove('loggedIn');
    this.storage.remove('loggedUser');
  }
}
