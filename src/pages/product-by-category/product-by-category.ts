import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'page-product-by-category',
  templateUrl: 'product-by-category.html',
})
export class ProductByCategoryPage implements OnInit {
  category;
  categoryList;
  items;
  constructor(private http: HttpClient, public navCtrl: NavController, public navParams: NavParams) {
    this.category = navParams.data;
    console.log(this.category);
  }

  ngOnInit() {
    this.http.get('https://woocommerce-5a98e.firebaseio.com/categories.json').subscribe(data => {
      this.items = data;
      console.log(this.items);
      console.log(this.category);
      this.categoryList = this.items.filter((data1) => {
        return (data1.parent === this.category.id + 1); // becuase categories are stored from index 0
      })
      console.log(this.categoryList);
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductByCategoryPage');
  }

  itemClicked(cate) {
    console.log(cate);
  }

}
